package com.zenika.academy.barbajavas.wordle.web.model.dto;

import com.fasterxml.jackson.annotation.JsonGetter;

public class GuessDto {
    private String guess;

    public String getGuess() {
        return guess;
    }

}
