package com.zenika.academy.barbajavas.wordle.web.model.dto;

public class GameDto {

    private int maxAttempts;
    private int wordLength;

    public int getMaxAttempts() {
        return maxAttempts;
    }

    public int getWordLength() {
        return wordLength;
    }
}
