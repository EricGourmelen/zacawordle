package com.zenika.academy.barbajavas.wordle.domain.model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private final String tid;
    private String name;
    private final String email;

    public User(String tid, String name, String email) {
        this.tid = tid;
        this.name = name;
        this.email = email;
    }

    public String getTid() {
        return tid;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }
}
