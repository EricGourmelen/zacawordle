package com.zenika.academy.barbajavas.wordle.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.DictionaryService;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.ScrabbleApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Component
public class GameManager {

    private final DictionaryService dictionaryService;
    private final GameRepository gameRepository;

    @Autowired
    public GameManager(DictionaryService dictionaryService, GameRepository gameRepository) {
        this.dictionaryService = dictionaryService;
        this.gameRepository = gameRepository;
    }

    public Game startNewGame(int wordLength, int nbAttempts) {
        Game game = new Game(UUID.randomUUID().toString(), this.dictionaryService.getRandomWord(wordLength), nbAttempts);
        this.gameRepository.save(game);
        return game;
    }

    public Game startNewGameWithTid(int wordLength, int nbAttempts, String tid) {
        Game game = new Game(UUID.randomUUID().toString(), this.dictionaryService.getRandomWord(wordLength), nbAttempts, tid);
        this.gameRepository.save(game);
        return game;
    }

    public Game attempt(String gameTid, String word) throws IllegalWordException, BadLengthException {
        Game game = this.gameRepository.findByTid(gameTid)
                .orElseThrow(() -> new IllegalArgumentException("This game does not exist"));

        if(!this.dictionaryService.wordExists(word)) {
            throw new IllegalWordException();
        }
        if(word.length() != game.getWordLength()) {
            throw new BadLengthException();
        }
        
        game.guess(word);

        this.gameRepository.save(game);
        
        return game;
    }

    public Optional<Game> findById(String tid){
        return this.gameRepository.findByTid(tid);
    }

    public List<Game> findGameByUser(String tid) {
        return this.gameRepository.findGameByUser(tid);
    }
}
