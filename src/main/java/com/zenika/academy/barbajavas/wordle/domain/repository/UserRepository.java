package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.User;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserRepository {
    private Map<String, User> users = new HashMap<>();
    
    public void save(User user) {
        this.users.put(user.getTid(), user);
    }
    
    public Optional<User> findByTid(String tid) {
        return Optional.ofNullable(users.get(tid));
    }

    public List<User> findByName(String name) {
        List<User> userList = new ArrayList<>();
        users.forEach((k,v) -> {
            if (v.getName().equals(name))
                userList.add(v);
        });
        return userList;
    }

    public List<User> findByEmail(String email) {
        List<User> userList = new ArrayList<>();
        users.forEach((k,v) -> {
            if (v.getEmail().equals(email))
                userList.add(v);
        });
        return userList;
    }

    public boolean deleteUser(String tid){
        boolean result = false;
        if (users.get(tid) != null){
            users.remove(tid);
            result = true;
        }
        return result;
    }

    public boolean updateUser(String tid, String newName){
        boolean result;
        this.findByTid(tid).orElseThrow(() -> new IllegalArgumentException()).setName(newName);
        result = true;
        return result;
    }

}
