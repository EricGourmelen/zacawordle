package com.zenika.academy.barbajavas.wordle.web.model.dto;

public class UserDto {
    private String name;
    private String email;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
