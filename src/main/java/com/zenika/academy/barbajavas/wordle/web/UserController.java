package com.zenika.academy.barbajavas.wordle.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.application.UserManager;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.ScrabbleApiService;
import com.zenika.academy.barbajavas.wordle.domain.service.displayer.console.color.ConsoleColorDisplayer;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.web.model.dto.GameDto;
import com.zenika.academy.barbajavas.wordle.web.model.dto.GuessDto;
import com.zenika.academy.barbajavas.wordle.web.model.dto.UserDto;
import com.zenika.academy.barbajavas.wordle.web.model.dto.UserUpdateDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/")
public class UserController {
    UserManager userManager;
    UserRepository userRepository;
    GameManager gameManager;

    public UserController(UserManager userManager, UserRepository userRepository, GameManager gameManager) {
        this.userManager = userManager;
        this.userRepository = userRepository;
        this.gameManager = gameManager;
    }

    @PostMapping("/users")
    public ResponseEntity<User> CreateNewUser(@RequestBody UserDto userDto) {
        return new ResponseEntity<User>(userManager.createUser(userDto.getName(), userDto.getEmail()),HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> GetUser(@RequestParam Optional<String> name, @RequestParam Optional<String> email) {
        return new ResponseEntity<>(userManager.getUsers(name, email), HttpStatus.OK);
    }

    @DeleteMapping("/users/{tid}")
    public ResponseEntity DeleteUser(@PathVariable String tid) {
        return new ResponseEntity<>(userManager.deleteUser(tid), HttpStatus.OK);
    }

    @PutMapping("/users/{tid}")
    public ResponseEntity UpdateUser(@PathVariable String tid,@RequestBody UserUpdateDto user){
        return new ResponseEntity<>(userManager.updateUser(tid, user.getName()), HttpStatus.OK);
    }

    @PostMapping("/users/{tid}")
    public ResponseEntity<Game> CreateNewGameWithTidUser(@RequestBody GameDto gameDto,@PathVariable String tid) {
        ResponseEntity result = new ResponseEntity<Game>(HttpStatus.BAD_REQUEST);
        if (gameDto.getWordLength() > 3 && gameDto.getWordLength() < 12 && gameDto.getMaxAttempts() >= 2) {
            Game game = gameManager.startNewGameWithTid(gameDto.getWordLength(), gameDto.getMaxAttempts(), tid);
            result = new ResponseEntity<Game>(game, HttpStatus.CREATED);
        }
        return result;
    }

//
//    @PostMapping("/games/{tid}")
//    public ResponseEntity<Game> tryToGuessWord(@PathVariable String tid, @RequestBody GuessDto guess) throws BadLengthException, IllegalWordException {
//        ResponseEntity result;
//        if (gameRepository.findByTid(tid).isPresent()) {
//            if (gameManager.attempt(tid, guess.getGuess()) != null) {
//                result = new ResponseEntity<Game>(gameManager.attempt(tid, guess.getGuess()), HttpStatus.OK);
//            } else {
//                result = new ResponseEntity<Game>(HttpStatus.BAD_REQUEST);
//            }
//        } else {
//            result = new ResponseEntity<Game>(HttpStatus.NOT_FOUND);
//        }
//        return result;
//    }
//
//
//
//    @GetMapping("/games/randomWord")
//    public void GetRandomWord( @RequestParam("length") int length) throws JsonProcessingException {
//        scrabbleApiService.getRandomWord(length);
//    }
}
