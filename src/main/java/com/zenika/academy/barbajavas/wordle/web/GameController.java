package com.zenika.academy.barbajavas.wordle.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.ScrabbleApiService;
import com.zenika.academy.barbajavas.wordle.domain.service.displayer.console.color.ConsoleColorDisplayer;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.web.model.dto.GameDto;
import com.zenika.academy.barbajavas.wordle.web.model.dto.GuessDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Scanner;

@RestController
@RequestMapping("api/")
public class GameController {
    ConsoleColorDisplayer consoleColorDisplayer;
    GameManager gameManager;

    public GameController(ConsoleColorDisplayer consoleColorDisplayer, GameManager gameManager) {
        this.consoleColorDisplayer = consoleColorDisplayer;
        this.gameManager = gameManager;
    }

    @PostMapping("/games")
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<Game> CreateNewGame(@RequestBody GameDto gameDto) {
        ResponseEntity result = new ResponseEntity<Game>(HttpStatus.BAD_REQUEST);
        if (gameDto.getWordLength() > 3 && gameDto.getWordLength() < 12 && gameDto.getMaxAttempts() >= 2) {
            Game game = gameManager.startNewGame(gameDto.getWordLength(), gameDto.getMaxAttempts());
            result = new ResponseEntity<Game>(game, HttpStatus.CREATED);
        }
        return result;
    }

    @PostMapping("/games/{tid}")
    public ResponseEntity<Game> tryToGuessWord(@PathVariable String tid, @RequestBody GuessDto guess, @RequestHeader String TidUser) throws BadLengthException, IllegalWordException {
        ResponseEntity result;
        Optional<Game> optGame = gameManager.findById(tid);
        if (optGame.isPresent()) {
            if ((TidUser == null ||(TidUser != null && optGame.orElseThrow(() -> new IllegalArgumentException()).getUserTid().equals(TidUser)))
            ){
                if (gameManager.attempt(tid, guess.getGuess()) != null) {
                    result = new ResponseEntity<Game>(gameManager.attempt(tid, guess.getGuess()), HttpStatus.OK);
                } else {
                    result = new ResponseEntity<Game>(HttpStatus.BAD_REQUEST);
                }
            } else {
                result = new ResponseEntity<Game>(HttpStatus.BAD_REQUEST);
            }
        } else {
            result = new ResponseEntity<Game>(HttpStatus.NOT_FOUND);
        }
        return result;
    }

    @GetMapping("/games/{tid}")
    public ResponseEntity<Game> GetStateOfGame(@PathVariable String tid) {
        // bonne façon d'utiliser les optionnal car le code ne va pas compiler en cas d'oublie de gestion du cas vide
        return gameManager.findById(tid).map(g -> ResponseEntity.ok(g)).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/games")
    public ResponseEntity<List<Game>> GetGameByUser(@RequestParam("userTid") String userTid) {
        return new ResponseEntity<>( gameManager.findGameByUser(userTid), HttpStatus.OK);
    }
}
