package com.zenika.academy.barbajavas.wordle.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zenika.academy.barbajavas.wordle.web.model.dto.RandomWordDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Locale;
import java.util.Map;

@Profile("dictionnary-api")
@Component
public class ScrabbleApiService implements DictionaryService{
    private String language;
    private RestTemplate restTemplate;

    @Autowired
    public ScrabbleApiService(
            RestTemplateBuilder restTemplatebuilder,
            @Value("${wordle.scrabble-api-uri}") String scrabbleApiBaseUri,
            @Value("${wordle.language}") String language
    ) {
        this.restTemplate = restTemplatebuilder
                .rootUri(scrabbleApiBaseUri)
                .build();
        this.language = language.toLowerCase(Locale.ROOT);
    }


    public String getRandomWord(int length) {
        final ScrabbleApiResponse response = restTemplate.getForObject("/dictionaries/" + language + "/randomWord?length="+length, ScrabbleApiResponse.class);
        return response.word;
    }


    @Override
    public boolean wordExists(String word) {
        RestTemplate restTemplate = new RestTemplate();
        RandomWordDto randomWordDto = new RandomWordDto();
        ObjectMapper mapper = new ObjectMapper();
        boolean result = false;

        String ScrabbleResourceUrl
                = "https://scrabble-api.fly.dev/";
        ResponseEntity<String> response
                = restTemplate.getForEntity(ScrabbleResourceUrl + "/api/dictionaries/"+ this.language +"/words/"+ word ,String.class);
        System.out.println(response);
        try {
            randomWordDto = mapper.readValue(response.getBody(), randomWordDto.getClass());
            result = randomWordDto.isWordExists();
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }

    private record ScrabbleApiResponse(String word, boolean wordExists) {}
}
