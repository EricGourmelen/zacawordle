package com.zenika.academy.barbajavas.wordle.web.model.dto;

public class RandomWordDto {
    private String word;
    private boolean wordExists;

    public String getWord() {
        return word;
    }

    public boolean isWordExists() {
        return wordExists;
    }
}
