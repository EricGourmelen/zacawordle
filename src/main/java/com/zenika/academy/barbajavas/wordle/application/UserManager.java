package com.zenika.academy.barbajavas.wordle.application;

import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.DictionaryService;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserManager {

    private final UserRepository userRepository;

    public UserManager(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(String name, String email) {
        User user = new User(UUID.randomUUID().toString(), name, email);
        userRepository.save(user);
        return user;
    }

    public List<User> getUsers(Optional name, Optional email) {
        List<User> result = new ArrayList<>();
        if (name.isPresent()){
            result = userRepository.findByName(name.get().toString());
        } else if (email.isPresent()){
            result = userRepository.findByEmail(email.get().toString());
        } else {
            // gérer le cas ou tout est vide
        }
        return result;
    }

    public boolean deleteUser(String tid){
        return userRepository.deleteUser(tid);
    }

    public boolean updateUser(String tid, String name){
        return userRepository.updateUser(tid, name);
    }
}
