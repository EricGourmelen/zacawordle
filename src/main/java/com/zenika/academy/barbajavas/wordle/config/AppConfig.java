package com.zenika.academy.barbajavas.wordle.config;

import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {
//    @Bean
//    public I18n getI18n(@Value("${wordle.language}") String language) throws Exception {
//        return I18nFactory.getI18n(language);
//    }

    @Bean
    public RestTemplate getRestTemplate(RestTemplateBuilder restTemplateBuilder)  {
        return restTemplateBuilder.build();
    }
}
